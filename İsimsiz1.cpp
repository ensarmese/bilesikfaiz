#include <stdio.h>
#include <locale.h>

float power(float x, int y);
void print_entry(char *name, float principal, float rate, int max_years, int max_months, int interval);
void print_duration(int years, int months);
void print_money(float amount);

int main() {
	setlocale(LC_ALL, "Turkish");
    char name[20];
    float principal, rate, monthly_payment, total_interest = 0;
    int max_years, max_months, interval, total_payments = 0;

    // Get inputs from user
    printf(".....................................................\n.*.*.*.*.*Faiz hesaplay�c�s�na ho� geldiniz*.*.*.*.*. \n.....................................................\n\n");
    printf("Ad�n�z� giriniz: ");
    scanf("%s", name);
    printf("Kredi miktar�n� giriniz: ");
    scanf("%f", &principal);
    printf("Y�ll�k faiz oran�n� girin: ");
    scanf("%f", &rate);
    printf("Y�l i�inde kredi vadesi: ");
    scanf("%d", &max_years);
    printf("Ay i�inde kredi vadesi: ");
    scanf("%d", &max_months);
    printf("Yineleme aral���: ");
    scanf("%d", &interval);

    // Toplam faizi hesapla
    total_interest = (principal / 100) * (rate / 12) * max_years*12 + max_months;

    // Ayl�k �demeyi ve toplam �demeleri hesaplay�n
    for (int i = 0; i < max_years * 12 + max_months; i += interval) {
        monthly_payment = (principal * rate) / (1 - power(1 + rate, -(max_years * 12 + max_months)));
        total_payments += monthly_payment;
    }

    // Raporu yazd�r
    print_entry(name, principal, rate, max_years, max_months, interval);
    printf("Toplam faiz: ");
    print_money(total_interest);
    printf("\n");

    // S�reye g�re �demeleri yazd�r
    for (float i = 3.0f; i <= 18.0f; i += 3.0f) {
    print_duration((int)i / 12, (int)i % 12);
	printf("i�in �demeler ");
    
    printf(": ");
    print_money(total_payments * (i / 12 + (float)((int)i % 12) / 12.0f));
    printf("\n");
}


    return 0;
}

float power(float x, int y) {
    float result = 1;
    for (int i = 0; i < y; i++) {
        result *= x;
    }
    return result;
}

void print_entry(char *name, float principal, float rate, int max_years, int max_months, int interval) {
    printf("�sim: %s\n", name);
    printf("Anapara tutar�: ");
    print_money(principal);
    printf("\n");
    printf("Y�ll�k faiz oran�: %.2f%%\n", rate * 100);
    printf("Maksimum s�re: ");
    print_duration(max_years, max_months);
    printf("\n");
    printf("�deme aral���: %d ayl�k\n", interval);
}

void print_duration(int years, int months) {
    if (years > 0) {
        printf("%d y�l", years);
    }
    if (months > 0) {
        printf("%d ay ", months);
    }
}

void print_money(float amount) {
    printf("$%.2f", amount);
}

