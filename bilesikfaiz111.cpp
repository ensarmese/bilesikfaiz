#include <stdio.h>
#include <locale.h>

int main() {
	setlocale(LC_ALL, "Turkish");
    // Kullanıcıdan girişleri al
    char name[50];
    printf("Please enter your name: ");
    scanf("%s", name);

    float loan_amount;
    printf("Loan amount: ");
    scanf("%f", &loan_amount);

    float interest_rate;
    printf("Interest rate (per year): ");
    scanf("%f", &interest_rate);

    int loan_term_years;
    printf("Loan term in years: ");
    scanf("%d", &loan_term_years);

    int loan_term_months;
    printf("Loan term in months: ");
    scanf("%d", &loan_term_months);

    int iteration_months;
    printf("Iteration in months: ");
    scanf("%d", &iteration_months);

    // Hesaplamaları yap
    float monthly_interest_rate = interest_rate / 12;
    int total_months = loan_term_years * 12 + loan_term_months;
    float power = 1;
    for (int i = 0; i < total_months; i++) {
        power *= (1 + monthly_interest_rate);
    }
    float monthly_payment = (loan_amount * monthly_interest_rate) / (1 - (1 / power));

    // Raporu yazdır
    printf("Report for %s:\n", name);
    printf("-------------------------------------------------------------------------------------\n");
    for (int month = 0; month < total_months; month += iteration_months) {
        int year = month / 12;
        int remaining_months = month % 12;
        float total_payment = monthly_payment * month;
        printf("-> year:%d, month:%d\n", year, remaining_months);
        printf("Total payment: %.1f$\n", total_payment);
        printf("Monthly payment: %.1f$\n", monthly_payment);
        printf("-------------------------------------------------------------------------------------\n");
    }

    return 0;
}

